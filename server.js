const braintree = require('braintree');
const cors = require('cors');
const express = require('express')
const app = express()
const port = 8001

app.use(cors());
app.use(express.json());

const gateway = new braintree.BraintreeGateway({
  environment: braintree.Environment.Sandbox,
  merchantId: "merchantId",
  publicKey: "publicKey",
  privateKey: "privateKey"
});

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.get('/client-token', (req, res) => {
  gateway.clientToken.generate({
    customerId: '1'
  }).then(response => {
    // pass clientToken to your front-end
    const clientToken = response.clientToken
    res.json({ clientToken: clientToken || null });
  });
});

app.post("/checkout", (req, res) => {
  console.log(req);
  const nonceFromTheClient = req.body.paymentMethodNonce;
  // Use payment method nonce here
  console.log(nonceFromTheClient);
  gateway.transaction.sale({
    amount: "10.00",
    paymentMethodNonce: nonceFromTheClient,
    // deviceData: deviceDataFromTheClient,
    options: {
      submitForSettlement: true
    }
  }).then(result => {
    console.log(result);
    res.json(result);
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});


