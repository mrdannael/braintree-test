import React, { useEffect, useState } from 'react';
import axios from 'axios';
import dropin from 'braintree-web-drop-in';
import './App.css';

function App() {
  const [message, setMessage] = useState('');

  useEffect(() => {
    fetch('http://localhost:8001/client-token').then(response => response.json()).then(({ clientToken }) => {
    var button = document.querySelector('#submit-button');

    dropin.create({
      authorization: clientToken,
      container: '#dropin-container',
      paypal: {
        flow: 'vault'
      }
    }, function (createErr, instance) {
      console.log(createErr);
      button?.addEventListener('click', function () {
      instance?.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
        console.log(requestPaymentMethodErr);
        axios.post<any>('http://localhost:8001/checkout', { paymentMethodNonce: payload.nonce }).then((response) => {
          console.log(response.data);
          const { success } = response.data;
          instance.teardown(function (teardownErr) {
            if (teardownErr) {
              console.error('Could not tear down Drop-in UI!');
            } else {
              console.info('Drop-in UI has been torn down!');
              document.getElementById('#submit-button')?.remove();
            }
          });

          if (success) {
            setMessage('<h1>Success</h1><p>Your Drop-in UI is working! Check your <a href="https://sandbox.braintreegateway.com/login">sandbox Control Panel</a> for your test transactions.</p><p>Refresh to try another transaction.</p>');
          } else {
            console.log(response.data);
            setMessage('<h1>Error</h1><p>Check your console.</p>');
          }
        })
      });
    });
  });
    });
  }, []);

  return (
    <div className="App">
      <div id="dropin-wrapper">
        <div id="checkout-message" dangerouslySetInnerHTML={{ __html: message }} />
        <div id="dropin-container"></div>
        <button id="submit-button">Submit payment</button>
      </div>
    </div>
  );
}

export default App;
